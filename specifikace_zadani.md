Jako zadání sememestrální práci jsem si vybral téma: "1. jednoduchá digitální hra"

Konkrétně jsem se rozhodl naprogramovat známou hru **BreakOut na Arduino Esploru**.
Hra spočívá v kontrole plošiny, která odráží míček, který následně rozbíjí
barikády zvyšující celkové skóre. Cíl této hry je zamezit spadnutí míčku pod osu 
ovládané plošiny a zároveň dosáhnout co nejvyššího skóre.

Hra bude obsahovat základní menu s logem hry, možností započat novou hru a zobrazení
dosavadních top skóre s iniciály rekordmanů. Po započatí se vyrendruje plošina, 
míček, barikády a počítadlo skóre. Po spadnutí míčku bude mít hráč možnost uložit
své skóre a hru restartovat.


K realizaci se chystám využít tyto komponenty Arduino Esplora:
1. display - ke zobrazení průběhu hry a dosaženého skóre
2. posuvný slider - k ovládání plošiny
3. tlačítka - posun v menu, odstartování hry a restart hry
4. \* EEPROM - k uložení top 3 dosažených rekordů s iniciáli hráče


*tento extension bude přidán jako poslední a bude přidán na základě 
průběhu implementace prvnotních funkcí
