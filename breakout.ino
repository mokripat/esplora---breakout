#include <Esplora.h>
#include <EEPROM.h>
#include <TFT.h>
#include <SPI.h>

// ARDUINO ESPLORA BREAKOUT! source code
// CREATED FOR BI-ARD 2019/20 by @mokripat
// Have fun!

// used technologies
// beeper     - for sound effects
// buttons    - for menu and ready confirmation
// screen     - visualization
// slider     - movement of paddle
// EEPROM     - to save best scores even after losing power

//HOW TO PLAY
//Menu:
//move -> up/down switch
//confirm -> right_button
//Game:
//move -> slider
//ready confirm -> right_button
//sound mute -> down_button

//KNOWN ISSUES
//buzzer sometimes goes crazy
//very rarely paddle is drawn after ready confirm

//------------------------------------------------------------------------------------------//
//                             EEPROM read/write FUNCTIONS

//This function will write a 4 byte (32bit) long to the eeprom at
//the specified address to adress + 3.
void EEPROMWritelong(int address, long value) {
  //Decomposition from a long to 4 bytes by using bitshift.
  //One = Most significant -> Four = Least significant byte
  byte four = (value & 0xFF);
  byte three = ((value >> 8) & 0xFF);
  byte two = ((value >> 16) & 0xFF);
  byte one = ((value >> 24) & 0xFF);

  //Write the 4 bytes into the eeprom memory.
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);
}

//This function will return a 4 byte (32bit) long from the eeprom
//at the specified address to adress + 3.
long EEPROMReadlong(long address) {
  //Read the 4 bytes from the eeprom memory.
  long four = EEPROM.read(address);
  long three = EEPROM.read(address + 1);
  long two = EEPROM.read(address + 2);
  long one = EEPROM.read(address + 3);

  //Return the recomposed long by using bitshift.
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}

//------------------------------------------------------------------------------------------//
//                             VARIABLES AND CONSTANTS
int screenX = EsploraTFT.width();
int screenY = EsploraTFT.height();

int ballX = screenX / 2;    //ball start position
int ballY = screenY / 2 + 30;
int ballLastX = ballX;      //position for erasing the ball
int ballLastY = ballY;
int ballXDir = 1;           //ball direction across screen
int ballYDir = -2;          //ball direction and speed, up and down screen
int ballRadius = 5;         //ball size
int ballXmax = 4;           //ball max speed left to right

unsigned long time;         //time managment
unsigned long waitUntil;

int lifes = 3;       //life counter
int score = 0;       //score counter
int lastScore = 0;
int gameRunning;     //track if the game is currently running

int ballDelay = 30;                      //milliseconds to wait to refresh ball position
int paddleX = screenX / 2;               //paddle start position
int paddleY = screenY - 5;
int paddleLastX = ballX;                 //gives the last paddle position something
int paddleW = 30;                        //width of paddle

const int bricksXoffset = 10;            //bricks X margin
const int bricksYoffset = 15;            //bricks Y margin
const int bricksWide = 14;               //number of bricks across the screen
const int bricksTall = 10;               //number of bricks down the screen
const int brickW = 10;                   //width of bricks in pixels
const int brickH = 5;                    //highth of brick in pixels
int totalBricks = 0;                     //tracks how many bricks are drawn on the screen
int bricksHit = 0;                       //track how many bricks have been hit
boolean brick[bricksWide][bricksTall];   //tracks if each individual brick is active
boolean sound = HIGH;                    //a flag to turn sound on/off
//------------------------------------------------------------------------------------------//

void setup(){
  Serial.begin(9600);
  EsploraTFT.begin();           //initialize the display
  EsploraTFT.background(0,0,0); //set the background the black
  gameRunning = 0;              //the game is not running yet
  mainMenu();                   //jump to mainMenu
}

void loop(){                   
  if(gameRunning == 1)          //if its game over, jump to mainMenu
    gameLoop();
  else
    mainMenu();
}

void mainMenu() {
  printMenu();                                              //prints logo and options
  boolean pos = true;                                       //position of arrow in menu
  printArrow(pos);                                          //prints arrow at correct position
  boolean choiceMade = false;                               
  boolean choice = true;                                    //true -> game ; false -> highscores
  while(choiceMade == false) {                              //till choice is made, waiting (looping)
    if(Esplora.readButton(SWITCH_RIGHT) == LOW) {           //confirmation
      choiceMade = true;
      delay(200);  
    } else if(Esplora.readButton(SWITCH_UP) == LOW || Esplora.readButton(SWITCH_DOWN) == LOW) { // arrow move
      pos = !pos;
      choice = !choice;
      printArrow(pos);
      Esplora.tone(300,50);
      delay(200);
    }
  }

  if(!choice) {               //highscores chosen
    showHighscores();         //show Highscores
    mainMenu();               //jump back to mainMenu
    return;
  }

  gameRunning = 1;            //else start a new game!
  newGameSound();
  newScreen();
  
}

void newGameSound() {
  if (sound == HIGH){
      Esplora.tone(150,300);
      delay(200);
      Esplora.tone(185,300);
      delay(200);
      Esplora.tone(205,300);
      delay(200);
      Esplora.tone(315,150);
      delay(150);
      Esplora.tone(230,200);
      delay(150);
      Esplora.tone(250,350);
      delay(300);
    }
}

void showHighscores() {

  EsploraTFT.background(0,0,0);
  EsploraTFT.stroke(0,255,255);
  EsploraTFT.setTextSize(2);
  EsploraTFT.text("BEST",screenX / 2 - 25,15);
  EsploraTFT.text("BREAKERS",35,35);
  EsploraTFT.stroke(0,0,255);
  EsploraTFT.setTextSize(1);
  EsploraTFT.line(50,32,screenX - 50,32);
  EsploraTFT.line(25,52,screenX - 25,52);
  
  char firstHS[24];
  long eepRead = EEPROMReadlong(0);  //read of first best highscore and converting it to string
  if(eepRead == -1) {                //if there is no data (it would read -1), convert to 0
    eepRead = 0;
  }
  itoa(eepRead, firstHS, 10);       
  
  char secondHS[24];
  eepRead = EEPROMReadlong(4);      //read of first best highscore and converting it to string
  if(eepRead == -1) {               //if there is no data (it would read -1), convert to 0
    eepRead = 0;
  }
  itoa(eepRead, secondHS, 10);      
  
  char thirdHS[24];
  eepRead = EEPROMReadlong(8);      //read of third best highscore and converting it to string
  if(eepRead == -1) {               //if there is no data (it would read -1), convert to 0
    eepRead = 0;
  }
  itoa(eepRead, thirdHS, 10);

  EsploraTFT.text("1.",35,65);
  EsploraTFT.text("2.",35,80);
  EsploraTFT.text("3.",35,95);

  EsploraTFT.stroke(0,255,255);
  EsploraTFT.text(firstHS,65,65);   //showing read scores from EEPROM
  EsploraTFT.text(secondHS,65,80);
  EsploraTFT.text(thirdHS,65,95);

  EsploraTFT.stroke(0,0,0);

  int readyButton = Esplora.readButton(SWITCH_RIGHT);  //ready confirmation
  while(readyButton == HIGH) {
    readyButton = Esplora.readButton(SWITCH_RIGHT);
  }
  confirmButtonSound();
}

void printArrow(boolean arrPosition) {      //print function for menu arrow
  EsploraTFT.stroke(0,0,0);
  EsploraTFT.text(">",screenX / 2 - 65,70); //erase old arrow
  EsploraTFT.text(">",screenX / 2 - 45,85);

  if(arrPosition) {                         //print arrow at correct position
    EsploraTFT.stroke(0,239,40);
    EsploraTFT.text(">",screenX / 2 - 65,70);
  } else {
    EsploraTFT.stroke(224,127,40);
    EsploraTFT.text(">",screenX / 2 - 45,85);
  }

  EsploraTFT.stroke(0,0,0);
}

void printMenu() {                      // prints logo and menu options, quite long for rainbow effect
  EsploraTFT.background(0,0,0);
  EsploraTFT.setTextSize(3);
  EsploraTFT.stroke(0,239,40);
  EsploraTFT.text("B",13,30);
  EsploraTFT.stroke(32,223,40);
  EsploraTFT.text("R",30,30);
  EsploraTFT.stroke(64,207,40);
  EsploraTFT.text("E",47,30);
  EsploraTFT.stroke(96,191,40);
  EsploraTFT.text("A",64,30);
  EsploraTFT.stroke(128,175,40);
  EsploraTFT.text("K",81,30);
  EsploraTFT.stroke(160,159,40);
  EsploraTFT.text("O",98,30);
  EsploraTFT.stroke(192,143,40);
  EsploraTFT.text("U",115,30);
  EsploraTFT.stroke(224,127,40);
  EsploraTFT.text("T",132,30);
  EsploraTFT.setTextSize(1);
  EsploraTFT.stroke(0,0,255);
  EsploraTFT.text("@by mokripat",screenX / 2 - 35,110);
  EsploraTFT.stroke(255,255,255);
  EsploraTFT.text("Start a new game!",screenX / 2 - 50,70);
  EsploraTFT.text("Highscores",screenX / 2 - 30,85); 
  EsploraTFT.stroke(0,0,0);
}

void gameLoop() {
   paddle();                   //updating paddle position
   checkSound();               //checks for on/of audio
   updateScore();
    
   //this section tracks the time and only runs at intervals set by ballDelay
   time = millis();
   if (time > waitUntil){          //when its time to refresh the ball
     waitUntil = time + ballDelay; //add the delay until the next refresh cycle
     moveBall();                   //routine to calculate new position and if the ball runs into anything
   } 
}

void updateScore(){                        //updates score
  if(lastScore != score) {
    char scoreUpdateScore[16];
    itoa(lastScore, scoreUpdateScore, 10); //integet to string
    
    EsploraTFT.stroke(0,0,0);              //deletes old score
    EsploraTFT.text("Score:",5,5);
    EsploraTFT.text(scoreUpdateScore,40,5); 
    
    itoa(score, scoreUpdateScore, 10);     //prints new score
    EsploraTFT.stroke(0,239,40);
    EsploraTFT.text("Score:",5,5);
    EsploraTFT.text(scoreUpdateScore,40,5);

    EsploraTFT.stroke(0,0,0);
    
    lastScore = score;
  }  
}

void pauseMenu() {                         //ready? function
  EsploraTFT.stroke(255,255,255);
  EsploraTFT.text("Ready?",screenX / 2 - 15,80);
  char lifeString[16];
  itoa(lifes, lifeString, 10);
  EsploraTFT.text("Lifes remaining:",screenX / 2 - 50,100);
  EsploraTFT.text(lifeString,screenX / 2 + 50,100);
  

  int readyButton = Esplora.readButton(SWITCH_RIGHT);  //ready confirmation
  while(readyButton == HIGH) {
    readyButton = Esplora.readButton(SWITCH_RIGHT);
  }
  confirmButtonSound();
  
  EsploraTFT.stroke(0,0,0);                            //erase and get game going
  EsploraTFT.text("Ready?",screenX / 2 - 15,80);
  EsploraTFT.text("Lifes remaining:",screenX / 2 - 50,100);
  EsploraTFT.text(lifeString,screenX / 2 + 50,100);
  gameRunning = 1;
}

void gameOverScreen() {                                //gameOverScreen with current score
  EsploraTFT.background(0,0,0);
  EsploraTFT.stroke(0,0,255);
  EsploraTFT.setTextSize(2);
  EsploraTFT.text("GAME OVER",27,screenY / 2 - 15);    //gameOver logo
  EsploraTFT.setTextSize(1);                          
  char scoreString[16];                                //score int to string
  itoa(score, scoreString, 10);
  EsploraTFT.text("Score: ",screenX / 2 - 35,80);      //prints final score
  EsploraTFT.text(scoreString,screenX / 2 + 10,80);

  if(!updateHighscores(score)){       //if new highscore was set, print New highscore set! and happy buzzer noices
    gameOverSound();
  } else {
    newHighScoreSound();
    for (int i = 0; i < 200; i++) {                    //new highscore text
      EsploraTFT.stroke(0,255,255);
      EsploraTFT.text("New highscore set!",screenX / 2 - 50,100);
      EsploraTFT.stroke(153,255,255);
      EsploraTFT.text("New highscore set!",screenX / 2 - 50,100);
    }
  }
  
  int readyButton = Esplora.readButton(SWITCH_RIGHT);  //waits for confirmation
  while(readyButton == HIGH) {
    readyButton = Esplora.readButton(SWITCH_RIGHT);
  }
  confirmButtonSound();
  EsploraTFT.stroke(0,0,0);                            
  lifes = 3;
  mainMenu();                                          //jump to mainMenu and lifes reset
}

bool updateHighscores(long result) {                   //after game is over, checks EEPROM for best results, if final score is higher than one of the best -> overwrite it
  for (int i = 0; i < 3; i++) {
    if(result > EEPROMReadlong( i * 4)){
      EEPROMWritelong( i * 4,result);
      return true;
    }
  }
  return false;
}

void gameOverSound() {
  if (sound == HIGH){   
    Esplora.tone(350,250);
    delay(250);
    Esplora.tone(275,250);
    delay(250);
    Esplora.tone(170,500);
  }
}

void newHighScoreSound() {
  if (sound == HIGH) {
    Esplora.tone(250,200);
    delay(250);
    Esplora.tone(350,300);
    delay(400);
    Esplora.tone(275,250);
    delay(300);
    Esplora.tone(375,350);
    delay(450);
    Esplora.tone(300,300);
    delay(350);
    Esplora.tone(375,100);
    delay(150);
    Esplora.tone(400,200);
    delay(400);
  }
}

void confirmButtonSound() {
  if (sound == HIGH){
    Esplora.tone(350,50);
    delay(100);
    Esplora.tone(300,100);
    delay(100);
    Esplora.tone(450,100);
  }
}

void paddle(){
  //read the slider, map it to the screen width, then subtract the witdh of the paddle
  //this gives us the position relative the left corner of the paddle
  paddleX=map(Esplora.readSlider(),0,1023,screenX,0)-paddleW/2;
  if (paddleX<1){  //if the paddle tries to go too far left
    paddleX=1;     //position it on the far left
  }
  if (paddleX > screenX - paddleW){                             //paddle optimilization so it wont go offscreen
    paddleX = screenX - paddleW;                                    
  }
  //paddle move
  if (abs(paddleX - paddleLastX) > 2) {                         //if paddleX changed, render new paddle, abs is used to reduce flickering
    EsploraTFT.fill(0,0,0);                                     //erase the old paddle
    EsploraTFT.rect(paddleLastX,paddleY,paddleW,4);
    EsploraTFT.fill(0,0,255);                               //draw the new paddle
    EsploraTFT.rect(paddleX,paddleY,paddleW,4);
    paddleLastX=paddleX;                                        //save new paddleX
  }
}

void checkSound(void){
  if (Esplora.readButton(SWITCH_LEFT) == LOW){    //if this button is pressed
    sound = !sound;                               //toggle the sound flag on/off
    delay(250);                                   //a little delay to release the button
  }
}

void moveBall(void){
  //check if the ball hits the side walls
  if (ballX < ballXDir * -1 | ballX > screenX - ballXDir - ballRadius){
    ballXDir = -ballXDir;
    if (sound == HIGH){
      Esplora.tone(230,10);
    }
  }
  //check if the ball hits the top of the screen
  if (ballY < ballYDir * -1){
    ballYDir = -ballYDir;
    if (sound == HIGH){
      Esplora.tone(730,10);
    }
  }
  //check if ball hits bottom of bricks
  //we run through the array, if the brick is active, check its position against the balls position
  for (int a = 0; a < bricksWide; a++){
    for (int b = 0; b < bricksTall; b++){
      if (brick[a][b]==HIGH){
        if (ballX > a * brickW + bricksXoffset - ballRadius & ballX < a * brickW + brickW + bricksXoffset & ballY > b * brickH + bricksYoffset - ballRadius & ballY < b * brickH + brickH + bricksYoffset){ //if brick was hit
          EsploraTFT.fill(0,0,0);                                                                         //erase the brick
          EsploraTFT.rect(a*brickW+bricksXoffset,b*brickH+bricksYoffset,brickW,brickH);
          brick[a][b] = LOW;                                                                              //set the brick inactive in the array
          ballYDir = -ballYDir;                                                                           //change ball direction
          bricksHit = bricksHit+1;                                                                        //add to the bricks hit count
          score += 5;                                                                                     //add score for brick
          if (sound == HIGH){                                                                             //brick-hit soundEffect
            Esplora.tone(130,10);
          }
        }
      }
    }
  }
  
  //check if the ball hits the paddle
  if (ballX > paddleX - ballXDir - ballRadius & ballX < paddleX + paddleW + ballXDir * -1 & ballY > paddleY - ballYDir - ballRadius & ballY < paddleY){
    ballXDir = ballXDir -(((paddleX + paddleW/2 - ballRadius/2)- ballX) * 0.3);    //change ball angle in relation to hitting the paddle
    if (ballXDir < -ballXmax){                                                     //speed limiter
      ballXDir = -ballXmax;
    }
    else if (ballXDir > ballXmax){                                                 //this wont allow the ball to go too fast left/right
      ballXDir = ballXmax;
    }
    ballYDir = -ballYDir;                                                          //change direction up/down
    if (sound == HIGH){                                                            //paddle-bounce soundEffect
      Esplora.tone(730,10);                                                           
    }
  }
  
  //check if the ball went past the paddle
  if (ballY > paddleY+10){
    lifes -= 1;                                                                    //decreasing lifes
    if(lifes) {                                                                    
      if (sound == HIGH){                                                          //ball-drop soundEffect
        Esplora.tone(130,1000);
      }
      for (int i = 0; i < 200; i++){
        EsploraTFT.stroke(0,150,20);
        EsploraTFT.text("WHOOPS",screenX / 2 - 15,100);
        EsploraTFT.stroke(150,0,20);
        EsploraTFT.text("WHOOPS",screenX / 2 - 15,100);
      }
      EsploraTFT.stroke(0,0,0);
      
    
    } else {                                                                       //if lifes drop to zero -> gameOver
      gameOverScreen();
      score = 0;
    }

    delay(2000);
    newScreen();
    ballY = screenY / 2 + 30;
    ballX = screenX / 2;
    ballXDir = 1;
    ballYDir = -abs(ballYDir);                                                     //due to difficult start, ball will always start going up
    bricksHit = 0;
  }
  
  //Game winning conditions -> all bricks were destroyed -> max score of current round!
  if (bricksHit == totalBricks){
    EsploraTFT.fill(0,0,0);
    EsploraTFT.rect(ballX,ballY,ballRadius,ballRadius);
    if (sound == HIGH){                                       //perfect round soundEffect
      Esplora.tone(400,250);
      delay(250);
      Esplora.tone(415,250);
      delay(250);
      Esplora.tone(430,500);
    }
    for (int i = 0 ; i < 200; i++){
      EsploraTFT.stroke(0,255,0);
      EsploraTFT.text("MAX SCORE!",screenX / 2 - 22,100);
      EsploraTFT.stroke(0,0,255);
      EsploraTFT.text("MAX SCORE!",screenX / 2 - 22,100);
    }
    EsploraTFT.stroke(0,0,0);
    newScreen();
    ballY = screenY / 2;
    ballX = screenX / 2;
    bricksHit = 0;
  }
  //calculate the new position for the ball
  ballX = ballX + ballXDir;                                     //move the ball x
  ballY = ballY + ballYDir;                                     //move the ball y
  EsploraTFT.fill(0,0,0);                                       //erase old ball
  EsploraTFT.rect(ballLastX,ballLastY,ballRadius,ballRadius);
  EsploraTFT.fill(0,255,255);                                 //draw the new ball
  EsploraTFT.rect(ballX, ballY,ballRadius,ballRadius);
  ballLastX = ballX;                                            //update the last ball position to the new ball position
  ballLastY = ballY;
}

void newScreen(void){            //this is the setup for clearing the screen for a new game
  EsploraTFT.background(0,0,0);  //set the screen black
  paddleLastX = ballX;           //set the last paddle position to something (makes the game draw a new paddle every time)
  paddle();                      //draws the paddle on the screen
  blocks();                      //draws the bricks on the screen

  pauseMenu();                   //readyCheck
}

//creates and prints all blocks on screen
void blocks(void){
  //assign the individual bricks to active in an array
  totalBricks = 0;
  for (int a = 0; a < bricksWide; a++){
    for (int b = 0; b < bricksTall; b++){
      brick[a][b] = HIGH;
    }
  }
  //now run trough the array and draw the bricks on the screen
  for (int a = 0; a < bricksWide; a++){
    for (int b = 0; b < bricksTall; b++){
      int c = map(b,0,bricksWide,0,255);
      if (brick[a][b] == HIGH) {
        totalBricks += 1;
        EsploraTFT.fill(c,255-c/2,40);
        EsploraTFT.rect(a * brickW + bricksXoffset,b * brickH + bricksYoffset,brickW,brickH);
      }
    }
  }
}

//core game build inspirated from lfmiller27, extended and optimilized
